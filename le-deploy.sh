#! /usr/bin/bash
set -eu

# Place this file in the directory /etc/letsencrypt/renewal-hooks/deploy/

DESTDIR=/opt/mailcow-dockerized/data/assets/ssl

cd "$RENEWED_LINEAGE"
cat cert.pem chain.pem > "$DESTDIR/cert.pem"
cp privkey.pem "$DESTDIR/key.pem"

docker restart "$(docker ps -qaf name=postfix-mailcow)"
docker restart "$(docker ps -qaf name=nginx-mailcow)"
docker restart "$(docker ps -qaf name=dovecot-mailcow)"
