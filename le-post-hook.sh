#! /bin/sh -eu
# Put this file in /usr/local/bin/ and it will be used with certbot renew --post-hook.
/sbin/nft flush chain inet filter le-temp
/bin/docker compose --project-directory /opt/mailcow-dockerized start nginx-mailcow 2>/dev/null
