## Troubleshooting & common tasks

### listing "compose projects"

```
root@mail-01:~# docker compose ls
NAME                STATUS              CONFIG FILES
mailcowdockerized   running(18)         /opt/mailcow-dockerized/docker-compose.yml
```

### listing running containers

```
docker compose --project-directory /opt/mailcow-dockerized ps
```

### tailing the postfix logs
```
docker compose --project-directory /opt/mailcow-dockerized logs --tail 30 -f postfix-mailcow

```

### amending postfix main.cf
```
vi /opt/mailcow-dockerized/data/conf/postfix/extra.cf
docker compose --project-directory /opt/mailcow-dockerized restart postfix-mailcow
```

### getting a shell in a container
```
docker compose --project-directory /opt/mailcow-dockerized exec postfix-mailcow bash -
```

### what's with all the containers?

[containerd](https://containerd.io/docs/) is running with default config in `/etc/containerd/config.toml`.

`dockerd` is talking to containerd thanks to `ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock`.

We have one docker compose project `mailcowdockerized`.
```
root@mail-01:~# docker compose --project-directory /opt/mailcow-dockerized ls
NAME                STATUS              CONFIG FILES
mailcowdockerized   running(18)         /opt/mailcow-dockerized/docker-compose.yml
```

`/opt/mailcow-dockerized/docker-compose.yml` lists all docker services, networks and containers.

TODO: Who's responsible for starting the containers? I guess containerd.
TODO: Who's responsible for setting up Docker's nftables rules? dockerd or containerd?

In any case, if you find that you've lost all the Docker nftables rules, systemctl restart docker will restore them.


### docker volumes

Listing docker volumes:
```
docker volume ls
```

Looking closer at `mailcowdockerized_vmail-vol-1` used by Dovecot for storing email.
Before 2023-06-21 (when https://git.glasklar.is/glasklar/services/mailcow/-/issues/15 was fixed) this volume used what's described as a stable but dirty trick in [mailcow documentation](https://docs.mailcow.email/manual-guides/Dovecot/u_e-dovecot-vmail-volume/).
That was not good, see above mentioned issue (#15), so now we're instead using the file `/opt/mailcow-dockerized/docker-compose.override.yml` to point directly at the directory `/var/vol/vmail1`:

```
root@mail-01:/opt/mailcow-dockerized# docker volume inspect mailcowdockerized_vmail-vol-1
[
    {
        "CreatedAt": "2023-06-21T22:04:03+02:00",
        "Driver": "local",
        "Labels": {
            "com.docker.compose.project": "mailcowdockerized",
            "com.docker.compose.version": "2.9.0",
            "com.docker.compose.volume": "vmail-vol-1"
        },
        "Mountpoint": "/var/lib/docker/volumes/mailcowdockerized_vmail-vol-1/_data",
        "Name": "mailcowdockerized_vmail-vol-1",
        "Options": {
            "device": "/var/vol/vmail1",
            "o": "bind",
            "type": "none"
        },
        "Scope": "local"
    }
]
```

### Rebuilding dovecot-uidlist files, one per mailbox

The uidlist file should have a header, one line, and one line per
email message in the mailbox.

How to check the status? The following command will for each mailbox
for USER print two lines, one with the number of lines in the uidlist
file and one with the number of files in the cur/ directory of the
mailbox.

    export user=USER; \
    docker compose --project-directory /opt/mailcow-dockerized exec \
      dovecot-mailcow doveadm mailbox list -u ${user}@glasklarteknik.se \
        | (while read -r mb; do \
          [[ $mb == INBOX ]] && mb=; \
          wc -l /var/vol/vmail1/glasklarteknik.se/${user}/Maildir/.${mb}/dovecot-uidlist; \
          ls /var/vol/vmail1/glasklarteknik.se/${user}/Maildir/.${mb}/cur | wc -l;\
        done)

Since the uidlist has a header line the number of lines in it should
be one more than the number of files in the mailbox.

Except that doesn't seem true for at least two INBOX mailboxes and one
Trash mailbox seen, where the line count in the uidlist is higher than
the number of files, with up to eight entries. Dont' know why.

Refererences:
- https://doc.dovecot.org/admin_manual/mailbox_formats/maildir/
- https://doc.dovecot.org/admin_manual/doveadm_mailbox_commands/


## Software updates
### Upgrading mailcow

```
root@mail-01:~# (cd /opt/mailcow-dockerized &&./update.sh)
```

### Checking for available updates
```
#! /bin/bash
set -eu

fn=$(mktemp mailcow-update-p.XXXXXXXXX)
function cleanup {
        [[ -f "$fn" ]] && rm "$fn"
}
cd /opt/mailcow-dockerized
trap cleanup EXIT
./update.sh --check > "$fn"
rc=$?
[[ $rc == 3 ]] && exit 0
echo "mailcow needs updating"
cat "$fn"
exit 1
```


### Installation info

Debian's Docker packages were not new enough when mail-01.so was
installed (2022-08-19) so we're (still) using packages from
docker.com:

    root@mail-01:~# cat /etc/apt/sources.list.d/docker.list
	deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian bookworm stable
