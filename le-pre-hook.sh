#! /bin/sh -eu
# Put this file in /usr/local/bin/ and it will be used with certbot renew --pre-hook.
/bin/docker compose --project-directory /opt/mailcow-dockerized stop nginx-mailcow 2>/dev/null
/sbin/nft add rule inet filter le-temp tcp dport 80 accept comment \"temp LE rule\"
