# mailcow

We're using [Mailcow](https://mailcow.email/) at https://mail.glasklartkenik.se/ for mailboxes and submission.

This repository collects user instructions, tips and tricks, and issues for tracking trouble we're having with the service.


Note that calendar functionality is also available over at https://nextcloud.glasklarteknik.se/ and that keeping track of multiple CalDAV endpoints might be a bit too much. Same for CardDAV contacts, really, even if the connection between email and contacts is pretty natural. TBD what to do here. 

## Webmail (SOGo)

The webmail service is reached by clicking the Webmail button at
https://mail.glasklarteknik.se/ or simply at
https://mail.glasklarteknik.se/SOGo/ directly.

### Automatic refresh and notifications

To enable automatic refreshing of new email and to enable
notifications, click the cogwheel symbol in the [left
panel](leftpanel.png) and update the "Refresh View" and "Enable
Desktop Notifications" settings.

## Resources

 - [sogo (webmail): Installation and Configuration Guide](https://www.sogo.nu/files/docs/SOGoInstallationGuide.html)
 - [sogo (webmail): Wiki](https://wiki.sogo.nu/)
